package com.example.htmlimages.modelui

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ImageUi(
    val uid: String,
    val url: String,
    val size: String,
    val weight: String
) : Parcelable