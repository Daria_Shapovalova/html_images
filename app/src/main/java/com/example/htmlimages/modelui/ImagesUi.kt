package com.example.htmlimages.modelui

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class ImagesUi(
    val images: List<ImageUi>
) : Parcelable