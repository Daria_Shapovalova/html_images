package com.example.htmlimages.recyclerview

import androidx.recyclerview.widget.DiffUtil
import com.example.htmlimages.modelui.ImageUi

class DiffCallback : DiffUtil.ItemCallback<ImageUi>() {
    override fun areItemsTheSame(oldItem: ImageUi, newItem: ImageUi): Boolean {
        return oldItem.uid == newItem.uid
    }

    override fun areContentsTheSame(oldItem: ImageUi, newItem: ImageUi): Boolean {
        return oldItem == newItem
    }
}