package com.example.htmlimages.recyclerview

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.example.htmlimages.R
import com.example.htmlimages.databinding.ItemImageBinding
import com.example.htmlimages.modelui.ImageUi
import java.io.File

class ImageRecyclerAdapter : RecyclerView.Adapter<ImageRecyclerAdapter.ViewHolder>() {
    private val differ = AsyncListDiffer(this, DiffCallback())

    var items: List<ImageUi>
        set(value) = differ.submitList(value)
        get() = differ.currentList

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    class ViewHolder private constructor(private val binding: ItemImageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ImageUi) {
            binding.imageView.layout(0, 0, 0, 0)

            Glide.with(binding.imageView).load(item.url).into(binding.imageView)

            binding.urlTextView.text = binding.imageView.resources.getString(R.string.url, item.url)

            Glide.with(binding.imageView)
                .asBitmap()
                .load(item.url)
                .into(object : CustomTarget<Bitmap>() {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                        val width = resource.width
                        val height = resource.height
                        binding.sizeTextView.text = binding.imageView.resources.getString(R.string.image_size, width, height)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {}
                })

            Glide.with(binding.imageView)
                .asFile()
                .load(item.url)
                .into(object : CustomTarget<File?>() {
                    override fun onResourceReady(resource: File, transition: Transition<in File?>?) {
                        val weight = resource.length().toDouble() / BYTES_IN_KILOBYTE
                        val weightString = "%.1f".format(weight)
                        binding.weightTextView.text = binding.imageView.resources.getString(R.string.image_weight, weightString)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {}
                })
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemImageBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }

            private const val BYTES_IN_KILOBYTE = 1024
        }
    }
}