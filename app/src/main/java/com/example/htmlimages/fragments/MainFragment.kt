package com.example.htmlimages.fragments

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.addTextChangedListener
import androidx.navigation.fragment.findNavController
import com.example.htmlimages.R
import com.example.htmlimages.databinding.MainFragmentBinding
import com.example.htmlimages.modelui.ImageUi
import com.example.htmlimages.modelui.ImagesUi
import com.example.htmlimages.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar

class MainFragment : Fragment() {
    private var _binding: MainFragmentBinding? = null
    val binding get() = _binding!!

    private lateinit var viewModel: MainViewModel
    private var url: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        setUpEditTextListener()
        setUpNextButtonOnClickListener()
        setUpResultObserver()
        setUpStatusObserver()
    }

    private fun setUpEditTextListener() {
        binding.linkEditText.addTextChangedListener { url = binding.linkEditText.text.toString() }
    }

    private fun setUpNextButtonOnClickListener() {
        binding.nextButton.setOnClickListener {
            closeKeyboard()
            if (url != "") {
                viewModel.loadHtml(url)
            } else {
                showSnackBar(R.string.error_empty_url)
            }
        }
    }

    private fun setUpResultObserver() {
        viewModel.result.observe(requireActivity()) {
            it?.let {
                if (it.isNotEmpty()) {
                    val images = it.map { image -> ImageUi(image, image, image, image) }
                    this.findNavController().navigate(MainFragmentDirections.actionShowImages(ImagesUi(images)))
                    viewModel.doneNavigating()
                } else {
                    showSnackBar(R.string.error_no_images)
                    viewModel.doneNavigating()
                }
            }
        }
    }

    private fun setUpStatusObserver() {
        viewModel.status.observe(requireActivity()) {
            when (it) {
                MainViewModel.LoadingStatus.LOADING -> binding.progressBar.visibility = View.VISIBLE
                MainViewModel.LoadingStatus.DONE -> binding.progressBar.visibility = View.GONE
                MainViewModel.LoadingStatus.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    showSnackBar(R.string.error_loading_page)
                }
                else -> {
                }
            }
        }
    }

    private fun closeKeyboard() {
        val view = requireActivity().currentFocus
        if (view != null) {
            val manager: InputMethodManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            manager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun showSnackBar(stringResource: Int?) {
        if (stringResource != null) {
            Snackbar.make(requireView(), stringResource, Snackbar.LENGTH_SHORT).show()
        }
    }
}