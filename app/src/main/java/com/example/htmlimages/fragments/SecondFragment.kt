package com.example.htmlimages.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.htmlimages.databinding.SecondFragmentBinding
import com.example.htmlimages.recyclerview.ImageRecyclerAdapter

class SecondFragment : Fragment() {
    private var _binding: SecondFragmentBinding? = null
    val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SecondFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val images = SecondFragmentArgs.fromBundle(requireArguments()).images

        val adapter = ImageRecyclerAdapter()
        binding.imagesRecyclerView.adapter = adapter
        adapter.items = images.images
    }
}