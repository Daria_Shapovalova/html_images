package com.example.htmlimages.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements

class MainViewModel : ViewModel() {

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _status = MutableLiveData<LoadingStatus>()
    val status: LiveData<LoadingStatus>
        get() = _status

    private val _result = MutableLiveData<List<String>?>()
    val result: LiveData<List<String>?>
        get() = _result

    fun loadHtml(url: String) {
        coroutineScope.launch {
            try {
                _status.value = LoadingStatus.LOADING
                val images: Elements = loadHtmlSuspend(url)
                _result.value = images
                    .map { it.absUrl(SRC_ATTRIBUTE_KEY).replace(HTTP_PREFIX, HTTPS_PREFIX) }
                    .filter { url -> url != "" && !url.endsWith(SVG_FILE_TYPE) }
                _status.value = LoadingStatus.DONE
            } catch (t: Throwable) {
                _status.value = LoadingStatus.ERROR
            }
        }
    }

    private suspend fun loadHtmlSuspend(url: String): Elements {
        return withContext(Dispatchers.IO) {
            val doc: Document = Jsoup.connect(url).get()
            doc.select(IMG_CSS_QUERY)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun doneNavigating() {
        _result.value = null
    }

    enum class LoadingStatus {
        LOADING, DONE, ERROR
    }

    companion object {
        private const val SRC_ATTRIBUTE_KEY = "src"
        private const val HTTP_PREFIX = "http:"
        private const val HTTPS_PREFIX = "https:"
        private const val SVG_FILE_TYPE = "svg"
        private const val IMG_CSS_QUERY = "img"
    }
}